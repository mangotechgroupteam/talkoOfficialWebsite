(function (moangoFunction) {
	function videofix(el){
		this.init(el);
	}
	videofix.prototype={
		init(el){
			this.$el = $(el);
			this.videoRatio = 1;
			this.$video = this.$el.find('video');
			$(window).on('resize',()=>{this.fixRatio();})
			$(window).on('load',()=>{this.fixRatio();})
			this.fixRatio();
		},
		fixRatio(){ 
			let videoRatio = 432/240;
			let frameRatio = this.$el.width() / this.$el.height();
			if(frameRatio>=videoRatio){
				this.$el.addClass('vertical');
				this.$el.removeClass('horizontal');
			}else{
				this.$el.removeClass('vertical');
				this.$el.addClass('horizontal');
			} 
		}
	}

	function headerCtrl(el) {
		this.init(el);
	}
	headerCtrl.prototype={
		init(el){
			this.$el = $(el);
			this.$anchor = $('[header-anchor]');
			if(this.$anchor.length==0){return false}
			$(window).on('scroll',()=>{
				if($(window).scrollTop()>this.getAnchorPos()){
					this.$el.addClass('stick');
				}else{
					this.$el.removeClass('stick');
				}
			})
		},
		getAnchorPos(){
			return this.$anchor.offset().top;
		}
	}

	function headerNav(el){
		this.init(el);
	}
	headerNav.prototype={
		init(el){
			this.$el=$(el);
			this.$nav = this.$el.find('nav');
			this.$menu = this.$el.find('[nav-menu]');
			this.isOpen=true;
			this.setNav();
		},
		setNav(){
			this.$nav.click(()=>{
				if(this.isOpen){
					this.$nav.addClass('active')
					this.$menu.addClass('active')
				}else{
					this.$nav.removeClass('active')
					this.$menu.removeClass('active')
				}
				this.isOpen = !this.isOpen;
			})
		}
	}

	function showsComponent(el){
		this.init(el);
	}
	showsComponent.prototype={
		init(el){
			this.$el=$(el);
			this.$btn = this.$el.find('[shows-btn]');
			this.$btn.click(()=>{
				this.$el.addClass('active');
			})
		}
	}

	function scrollHerf(el){
		this.init(el);
	}
	scrollHerf.prototype={
		init(el){
			this.$el = $(el);
			this.$el.click(()=>{
				this.goHerf();
			})
			$(document).ready(()=>{
				if(this.$el.attr('scroll-herf')==location.hash){this.goHerf();}
			});
		},
		goHerf(){
			let offset = (($(this.$el.attr('scroll-herf')).offset()||{}).top||0) - 53;
			$('body, html').animate({scrollTop:offset}, 800);
		}
	}

	function numberGrowing(el){
		this.init(el);
	}
	numberGrowing.prototype={
		init(el){
			this.$el = $(el);
			this.currentNumber = this.$el.attr('number-growing-start-from')||0;
			this.range = this.$el.attr('number-growing-range')||3000;
			this.finalNumber = this.$el.attr('number-growing');
			this.renderNumber(this.currentNumber);
		},
		start(){
			let infinite = setInterval(()=>{
				let TempNum = this.currentNumber + parseInt(Math.random()*this.range);
				if(TempNum>this.finalNumber){
					this.renderNumber(this.finalNumber);
					clearInterval(infinite);
				}else{
					this.renderNumber(TempNum);
				}
			},50)
		},
		renderNumber(num){
			this.currentNumber = num;
			this.$el.html(Intl.NumberFormat().format(num));
		}
	}

	function enquiryForm(el){
		this.init(el);
	}
	enquiryForm.prototype={
		init(el){
			this.$el = $(el);
			this.loading = mangoFunction.loadingComponent({text:'Sending...'})
			this.$el.submit((event)=>{
				event.preventDefault();
				let req = {};
				this.$el.find('input,textarea').each((index,item)=>{
					let $item = $(item);
					req[$item.attr('name')] = $item.val();
				});
				this.sendForm(req);
			})
		},
		renderTemplate(req){
			let html = '';
			html+='<p>姓名:'+req.name+'</p>';
			html+='<p>电话:'+req.tel+'</p>';
			html+='<p>微信号码:'+req.wechat+'</p>';
			html+='<p>邮箱地址:'+req.email+'</p>';
			html+='<p>留言:'+req.msg+'</p>';
			return html;
		},
		sendForm(req){
			this.loading.show();
			let body = this.renderTemplate(req);
			$.ajax({
	            type: "post",
	            url: "https://www.mangotechapi.com.au/api/email/send/",          
	            Contenttype: 'application/json;charset=utf-8',
	            dataType: 'json',
	            cache: false,       
	            data: {Receiver:[{EmailAddress:'shuo@talko.com.au'}],Subject:'订阅最新资讯-订阅者表单',Body:body},
	            beforeSend: function (xhr) {
	                xhr.setRequestHeader('Authorization', make_base_auth('mangotech', 'mangotech2017'));
	                xhr.setRequestHeader('API_KEY', 'mangotechkey2017');
	            },
	            fail:()=>{
	                alert('email sent fail');
					this.loading.close();
	            },
	            success:()=>{
	                alert('email sent');
					this.loading.close();
	            }
	    	});
		}
	}
	function make_base_auth(user, password) {
        var tok = user + ':' + password;
        var hash = btoa(tok);
        return "Basic " + hash;
    }

	moangoFunction.showsComponent = function(el='#showsComponent'){return new showsComponent(el)}
	moangoFunction.headerNav = function(el='header'){return new headerNav(el)}
	moangoFunction.headerCtrl = function(el='header'){return new headerCtrl(el)}
	moangoFunction.enquiryForm =function(el='#enquiryForm'){return new enquiryForm(el)}
	moangoFunction.scrollHerf = function(el='[scroll-herf]'){
		$(el).each((index,item)=>{
			new scrollHerf(item);
		});
	}
	moangoFunction.numberGrowing = function(el='[number-growing]'){
		let list = [];
		$(el).each((index,item)=>{
			list.push(new numberGrowing(item));
		});
		return list;
	}
	moangoFunction.videofix = function(el='.videofix'){
		$(el).each((index,item)=>{
			new videofix(item);
		});
	};
	window.moangoFunction=moangoFunction;
})(window.moangoFunction||{})